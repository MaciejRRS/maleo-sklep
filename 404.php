<?php
get_header();
?>

<div class="top-space"></div>

<!-- Section Breadcrumbs -->
<?php get_template_part( 'template-parts/commons/common', 'breadcrumbs' ); ?>

<div class="container not-found__container">
    <section class="text-center">

        <!-- <h1 class="not-found__title">404</h1> -->
        <img src="<?php the_field('404_image', 'options') ?>"
            alt=""
            class="error">

        <p class="information__404"><?php the_field('404_information', 'options') ?></p>

        <a class="btn__404"
            href="/"><?php the_field('404_button_text', 'options') ?></a>

    </section>
</div>
<?php

get_footer();
?>
<style>
section.text-center {
    padding-top: 0;
}

img.error {
    max-width: 900px;
    display: block;
    margin: 0 auto;
    width: 90%;
}

p.information__404 {
    margin-bottom: 30px;
    font-size: 25px;
    color: #373531;
    font-family: 'Nunito', sans-serif;
}

.btn__404 {
    background-color: #ECEBEA;
    width: fit-content;
    margin: 0 auto;
    font-size: 18px;
    text-transform: uppercase;
    color: #6A5D6B;
    padding: 18px 135px;
    font-family: 'Nunito', sans-serif;
    font-size: 25px;
}

.btn__404:hover {
    color: #6A5D6B;
}

.not-found__container {
    margin: 0px auto 100px;
}

.text-center {
    text-align: center !important;
}

.not-found__title {
    color: #cc273f;
    font-weight: 400;
    font-size: 5rem !important;
}

.not-found__subtitle {
    font-weight: 400;
    font-size: 2rem !important;
}


@media(max-width: 767px) {
    .btn__404 {
        font-size: 18px;
        padding: 15px 80px;
    }

    p.information__404 {
        font-size: 18px;
    }

}
</style>