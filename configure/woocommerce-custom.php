<?php
// var_dump('hello_world');


// Disable Woocommerce main styles
add_filter('woocommerce_enqueue_styles', '__return_false');


/**
 * Disable WooCommerce block styles (front-end).
 * https://themesharbor.com/disabling-css-styles-of-woocommerce-blocks/
 */
function slug_disable_woocommerce_block_styles() {
  wp_dequeue_style( 'wc-block-style' );
}
add_action( 'wp_enqueue_scripts', 'slug_disable_woocommerce_block_styles' );



/**
 * Disable WooCommerce block styles (back-end).
 * https://themesharbor.com/disabling-css-styles-of-woocommerce-blocks/
 */
function slug_disable_woocommerce_block_editor_styles() {
  wp_deregister_style( 'wc-block-editor' );
  wp_deregister_style( 'wc-block-style' );
}
add_action( 'enqueue_block_assets', 'slug_disable_woocommerce_block_editor_styles', 1, 1 );



// ADD ACF FIELDS TO WOOCOMMERCE ATTRIBUTES

// Adds a custom rule type.
add_filter( 'acf/location/rule_types', function( $choices ){
    $choices[ __("Other",'acf') ]['wc_prod_attr'] = 'WC Product Attribute';
    return $choices;
} );

// Adds custom rule values.
add_filter( 'acf/location/rule_values/wc_prod_attr', function( $choices ){
    foreach ( wc_get_attribute_taxonomies() as $attr ) {
        $pa_name = wc_attribute_taxonomy_name( $attr->attribute_name );
        $choices[ $pa_name ] = $attr->attribute_label;
    }
    return $choices;
} );

// Matching the custom rule.
add_filter( 'acf/location/rule_match/wc_prod_attr', function( $match, $rule, $options ){
    if ( isset( $options['taxonomy'] ) ) {
        if ( '==' === $rule['operator'] ) {
            $match = $rule['value'] === $options['taxonomy'];
        } elseif ( '!=' === $rule['operator'] ) {
            $match = $rule['value'] !== $options['taxonomy'];
        }
    }
    return $match;
}, 10, 3 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 0 );

remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_output_all_notices', 70 );




/**
 * Remove WooCommerce breadcrumbs 
 */
// add_action( 'init', 'my_remove_breadcrumbs' );
// function my_remove_breadcrumbs() {
//     remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
// }





//remove title form products!!!! page
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10); // Remove title form original position
// remove image/thumbnail from products page 
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
// remove prive form products page
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

add_action('woocommerce_shop_loop_item_title', 'abChangeProductsTitle', 10); // Insert title in new position ( out of main container )

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 70 );
function abChangeProductsTitle()
{

    global $product; 
    $pid = $product->get_id();
    $currency = get_woocommerce_currency_symbol();
    $add_to_wishlist = do_shortcode('[yith_wcwl_add_to_wishlist product_id=' . $pid . ']' );



echo '<div class="product__img">';
echo $add_to_wishlist;
    echo $product->get_image('large');
        echo '<div class="product__options">';
            echo '<div class="product__sizes">';
                echo '<p class="size">' . $product->get_attribute('rozmiar') . '</p>';
            echo '</div>';
            echo '<div class="product__colors">';
                echo '<p class="size">' . $product->get_attribute('kolor') . '</p>';
            echo '</div>';
        echo '</div>';
echo '</div>';

echo ' <div class="product__info info">';
echo '<p class="product__name">';
echo the_title();
echo '</p>';

echo ' <p class="product__price">';
echo $product->get_price();
echo $currency;
echo '</p>';
echo '</div>';

    };


    add_filter('yith_wcwl_is_wishlist_responsive', '__return_false');
add_filter('yith_wcwl_localize_script', function ($loc) {
    $loc['actions']['load_mobile_action'] = 'no-mob-ver';
    return $loc;
});
add_filter('yith_wcwl_template_part_hierarchy', function ($data) {
    foreach ($data as $k => $v) $data[$k] = str_replace('-mobile.php', '.php', $v);
    return $data;
});


// deleted add to cart button from products and category page
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);


// remove breadcrumbs from /sklep
add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
 
    if (is_shop() || is_archive())
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
 
}



/* Show pagination on the top of shop page */
add_action( 'woocommerce_before_shop_loop', 'woocommerce_pagination', 10 );

add_filter( 'wc_add_to_cart_message', 'custom_add_to_cart_message' );
function custom_add_to_cart_message() {
    global $woocommerce;

        $return_to  = get_permalink(woocommerce_get_page_id('shop'));
        $message    = sprintf('<a href="%s" class="button wc-forwards">%s</a> %s', $return_to, __('Continue Shopping', 'woocommerce'), __('Product successfully added to your cart.', 'woocommerce') );
    return $message;
}


function wc_remove_checkout_fields( $fields ) {

    // Billing fields

    unset( $fields['billing']['billing_state'] );
    // Shipping fields
    unset( $fields['shipping']['shipping_state'] );
    
    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'wc_remove_checkout_fields' );


add_filter( 'woocommerce_get_script_data', 'change_view_cart', 10, 2 );
function change_view_cart( $params, $handle ) {
  if ( $handle == 'wc-add-to-cart' ) {
    $params['i18n_view_cart'] = "Dodano do koszyka. Zobacz koszyk lub kontynuuj zakupy.";
  }
  
  return $params;
}

add_action( 'wp_footer' , 'custom_quantity_fields_script' );
function custom_quantity_fields_script(){
    ?>
<script type='text/javascript'>
jQuery(function($) {
    if (!String.prototype.getDecimals) {
        String.prototype.getDecimals = function() {
            var num = this,
                match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            if (!match) {
                return 0;
            }
            return Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
        }
    }
    // Quantity "plus" and "minus" buttons
    $(document.body).on('click', '.plus, .minus', function() {
        var $qty = $(this).closest('.quantity').find('.qty'),
            currentVal = parseFloat($qty.val()),
            max = parseFloat($qty.attr('max')),
            min = parseFloat($qty.attr('min')),
            step = $qty.attr('step');

        // Format values
        if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
        if (max === '' || max === 'NaN') max = '';
        if (min === '' || min === 'NaN') min = 0;
        if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;

        // Change the value
        if ($(this).is('.plus')) {
            if (max && (currentVal >= max)) {
                $qty.val(max);
            } else {
                $qty.val((currentVal + parseFloat(step)).toFixed(step.getDecimals()));
            }
        } else {
            if (min && (currentVal <= min)) {
                $qty.val(min);
            } else if (currentVal > 0) {
                $qty.val((currentVal - parseFloat(step)).toFixed(step.getDecimals()));
            }
        }

        // Trigger change event
        $qty.trigger('change');
    });
});
</script>
<?php
}



    ?>