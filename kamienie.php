<?php 
/* 
Template Name: Kamienie    
*/ 
?>


<?php get_header('shop') ?>


<main id="kamienie">

    <section class="kamienie-section">
        <div class="container">


            <div class="header-wrapper">
                <div class="header-breadcrumps">
                    <?php if (function_exists('bcn_display')) {
                bcn_display();
            } ?>
                </div>


                <div class="title-wrap">
                    <h1 class="page-title"><?php the_title(); ?></h1>
                </div>
            </div>


            <div class="grid-container">
                <?php

$custom_query = new WP_Query( 
    array(
    'post_type' => 'kamienie',
    'post_status'=>'publish',
    'posts_per_page' => -1,
    ) 
);

 if ( $custom_query->have_posts() ) : while  ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

                <!-- start block grid item -->
                <a class="link-post item" href="<?php echo get_permalink(); ?>">
                    <div class="img-area">
                        <?php the_post_thumbnail( 'large' ); ?>
                    </div>
                    <div class="title-section">
                        <h2><?php the_title(); ?></h2>
                    </div>
                    <div class="button-action">
                        <?php the_field('button_action_kamienie','options')?>


                        <svg xmlns="http://www.w3.org/2000/svg" width="13.15" height="11.508"
                            viewBox="0 0 13.15 11.508">
                            <g id="arrow-right-short" transform="translate(-9 -10.123)">
                                <path id="Path_588" data-name="Path 588"
                                    d="M18.24,10.365a.822.822,0,0,1,1.164,0L24.334,15.3a.822.822,0,0,1,0,1.164L19.4,21.39a.823.823,0,0,1-1.164-1.164l4.35-4.349-4.35-4.349a.822.822,0,0,1,0-1.164Z"
                                    transform="translate(-2.425 0)" fill="#32b1b6" fill-rule="evenodd" />
                                <path id="Path_589" data-name="Path 589"
                                    d="M9,17.7a.822.822,0,0,1,.822-.822H20.505a.822.822,0,0,1,0,1.644H9.822A.822.822,0,0,1,9,17.7Z"
                                    transform="translate(0 -1.82)" fill="#32b1b6" fill-rule="evenodd" />
                            </g>
                        </svg>
                    </div>
                </a>
                <!-- end block grid item -->


                <?php
                endwhile; 
                endif; 
                wp_reset_query();
                ?>


            </div>

        </div>
    </section>

    <!-- Info Left Section -->
    <?php get_template_part( 'template-parts/commons/common', 'social' ); ?>

</main>
















<?php get_footer('shop') ?>