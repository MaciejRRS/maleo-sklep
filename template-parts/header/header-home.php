<nav class="navbar menu">
    <div id="navbarTop"
        class="container container__small">
        <div class="row mx-0 row__top">

            <div class="left row mx-0">
                <a href="/moje-konto/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-user.php'; ?>
                </a>
                <a href="/wyszukiwarka/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-search.php'; ?>
                </a>
            </div>

            <div class="middle">
                <a href="/"
                    class="header__logo big">
                    <?php include 'svg-logo-header-big.php'; ?>
                </a>
                <a href="/"
                    class="header__logo small d-none">
                    <?php include 'svg-logo-header-small.php'; ?>
                </a>
            </div>

            <div class="right row mx-0">
                <a href="/wishlist/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-heart.php'; ?>
                </a>
                <a href="/koszyk/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-cart.php'; ?>
                    <span class="cart-number"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                </a>
                <button class="navbar-toggler first-button"
                    type="button"
                    id="menu-button">
                    <div class="animated-icon1"><span></span><span></span><span></span></div>
                </button>
            </div>
        </div>
    </div>

    <?php
        wp_nav_menu( array(
            'theme_location'    => 'primary_bottom',
            'depth'             => 2,
            'link_before'    => '<span class="socialmedia">',
            'link_after'     => '</span>',
            'container'         => 'div',
            'container_class'   => 'container-fluid',
            'container_id'      => 'navbarBottom',
            'menu_class'        => 'container row row__bottom',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker(),
        ) );
    ?>
</nav>

<nav id="navbarBurger"
    class="navbar burger">
    <!--<a href="/"
        class="header__logo big">
        <?php include 'svg-logo-header-big.php'; ?>
    </a>-->
    <?php
        wp_nav_menu( array(
            'theme_location'    => 'primary_bottom',
            'depth'             => 2,
            'link_before'    => '<span class="socialmedia">',
            'link_after'     => '</span>',
            'container'         => 'div',
            'container_class'   => 'container',
            'container_id'      => 'navbarBurger',
            'menu_class'        => 'nav__links',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker(),
        ) );
    ?>

</nav>



<nav id="navbarMobile"
    class="navbar__mobile d-none">
    <div class="container">
        <div class="row__nav mobile row mx-0">
            <a href=""
                id="burgerBtn"
                class="col-2 nav__link mobile burger__btn">
                <?php include 'svg-menu.php'; ?>
                <p class="link__name mobile ">menu</p>

            </a>
            <a href="#"
                class="col-2 nav__link mobile">
                <?php include 'svg-search.php'; ?>
                <p class="
                link__name
                mobile">szukaj</p>
            </a>

            <a href="#"
                class="col-2 nav__link mobile">

                <?php include 'svg-user.php'; ?>
                <p class="link__name mobile">konto</p>
            </a>

            <a href="#"
                class="col-2 nav__link mobile">
                <?php include 'svg-heart.php'; ?>
                <p class="link__name mobile">ulubione</p>
            </a>

            <a href="#"
                class="col-2 nav__link mobile">
                <?php include 'svg-cart.php'; ?>
                <p class="link__name mobile">koszyk</p>
            </a>

            <a href="#"
                class="col-2 nav__link mobile">
                <?php include 'svg-user.php'; ?>
                <p class="link__name mobile">atelier</p>
            </a>
        </div>
    </div>
</nav>

<nav id="navbarBottomBar">
    <div class="bottom">
        <div class="nav-list-btm">

            <div class="my-account">
                <a href="/moje-konto/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-user.php'; ?>
                    <p>konto</p>
                </a>
            </div>

            <div class="">
                <a href="/wyszukiwarka/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-search.php'; ?>
                    <p>szukaj</p>
                </a>
            </div>

            <div class="">
                <a href="/wishlist/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-heart.php'; ?>
                    <p>ulubione</p>
                </a>
            </div>

            <div class="">
                <a href="/koszyk/"
                    class="menu__link menu__top btn__circle">
                    <?php include 'svg-cart.php'; ?>
                    <p>koszyk</p>
                </a>
            </div>

            <div class="">
                <a href="/sklep/"
                    class="menu__link menu__top btn__circle">
                    <img src="/app/themes/maleo_rrs/assets/src/img/diamond.svg" />
                    <p class="atelier">atelier</p>
                </a>
            </div>
        </div>
    </div>
</nav>



<script>
$("#menu-button").on("click", function() {
    $("#navbarBurger").toggleClass("open");
})
</script>