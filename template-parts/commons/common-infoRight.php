<!-- ACF -> common info right -->
<!--
  Exist on pages: 
- home page
- basic template
 -->


<?php 
$infoRight = get_field('show_info_right_section'); 
if  ( $infoRight ) {
?>

<section class="info right">
    <div class="container right">
        <div class="title__wrap right">
            <h2 class="section__title info__title title right"><?php the_field('info_right_tytul') ?></h2>
        </div>
        <div class="info__content section__content content right text"><?php the_field('info_right_tresc') ?></div>
    </div>
</section>

<?php } ?>