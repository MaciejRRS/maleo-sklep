<?php
/*
 * Template Name: Home Template
 */
get_header();
?>

<div class="top-space"></div>

<?php 
    $introImageDesktop = get_field('intro_zdjecie_w_tle');
?>


<section class="intro"
    style="background-image: url(<?php echo $introImageDesktop ?>)">
    <div class="intro__banner banner">
        <h2 class="banner__text top"><?php the_field('intro_tekst_top') ?></h2>
        <h2 class="banner__text bottom"><?php the_field('intro_tekst_bottom') ?></h2>
        <a href="<?php the_field('intro_przycisk_link') ?>"
            class="banner__btn btn__circle">
            <?php include 'assets/src/img/icons/btn_arrow-right.php'; ?>
            <span><?php the_field('napis_zobacz', 'options') ?></span>
        </a>
    </div>
</section>






<section class="new new__products slider">
    <div class="container">
        <h2 class="section__title slider__title"><?php the_field('slider_title_homepage') ?></h2>

        <div class="slider slider__atelier">

        
            <?php
                $id_cat1 = get_field("homepage_category_slider");
                $args = array(
                    'post_type' => 'product',
                    'product_cat' =>  $id_cat1->name ,
                    'orderby' => 'date',
                    'posts_per_page' => -1,
                    
                );
                $loop = new WP_Query( $args );
                if ( $loop->have_posts() ) : $i=0;
                while ( $loop->have_posts() ) : $loop->the_post(); $i++; 
                global $product; 
                $pid = $product->get_id();
                $currency = get_woocommerce_currency_symbol();
                $add_to_wishlist = do_shortcode('[yith_wcwl_add_to_wishlist product_id=' . $pid . ']' );
            ?>
            
            
            <a href="<?php echo get_permalink( $loop->post->ID ) ?>"
                class="product__card product"
                target="_blank">
                <?php abChangeProductsTitle() ?>
            </a>

            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            
        </div>
    </div>



    </div>
</section>

<script>
$(document).ready(function() {
    $('.slider__news').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 567,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});
</script>


<section class="info-image left">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 images__wrap left">
                <div class="color__wrap left">
                    <img src="<?php the_field('home_info_image_left_zdjecie') ?>"
                        alt=""
                        class="info-image__img left">
                    <img src="<?php the_field('home_info_image_left_zdjecie_hover') ?>"
                        alt=""
                        class="info-image__img left hover">
                </div>
            </div>
            <div class="col-lg-6 content__wrap left">
                <h2 class="section__title info-image__title left"><?php the_field('home_info_image_left_tytul') ?></h2>
                <div class="info-image__content text">
                    <?php the_field('home_info_image_left_tresc') ?>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="info-image right">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 content__wrap">
                <h2 class="section__title info-image__title right"><?php the_field('info_image_right_tytul') ?></h2>
                <div class="info-image__content text">
                    <?php the_field('info_image_right_tresc') ?>
                </div>
            </div>
            <div class="col-lg-6 images__wrap left">
                <div class="color__wrap right">
                    <img src="<?php the_field('info_image_right_zdjecie') ?>"
                        alt=""
                        class="info-image__img right">
                    <img src="<?php the_field('info_image_right_zdjecie_hover') ?>"
                        alt=""
                        class="info-image__img right hover">
                </div>
            </div>

        </div>
    </div>
</section>


<section class="new new__products slider">
    <div class="container">
        <h2 class="section__title slider__title slider_second"><?php the_field('homepage_slider_title') ?></h2>

        <div class="slider slider__atelier">

        
            <?php
                $id_cat2 = get_field("homepage_category");
                $args = array(
                    'post_type' => 'product',
                    'product_cat' =>  $id_cat2->name ,
                    'orderby' => 'date',
                    'posts_per_page' => -1,
                    
                );
                $loop = new WP_Query( $args );
                if ( $loop->have_posts() ) : $i=0;
                while ( $loop->have_posts() ) : $loop->the_post(); $i++; 
                global $product; 
                $pid = $product->get_id();
                $currency = get_woocommerce_currency_symbol();
                $add_to_wishlist = do_shortcode('[yith_wcwl_add_to_wishlist product_id=' . $pid . ']' );
            ?>
            
            
            <a href="<?php echo get_permalink( $loop->post->ID ) ?>"
                class="product__card product"
                target="_blank">
                <?php abChangeProductsTitle() ?>
            </a>

            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            
        </div>



    </div>
</section>


<script>
$(document).ready(function() {
    $('.slider__atelier').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 567,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});
</script>

<!-- Info Left Section -->
<?php get_template_part( 'template-parts/commons/common', 'infoLeft' ); ?>


<section class="info-cards">
    <div class="container">
        <div class="row mx-0">
            <?php $infoCards = get_field('home_info_cards');
            if ( $infoCards ) {
                foreach( $infoCards as $card) { ?>

            <div class="info__card card col-md-4">
                <img src="<?php echo $card['info_card_icon'] ?>"
                    class="card__icon">
                <p class="card__name"><?php echo $card['info_card_text'] ?></p>
            </div>


            <?php }} ?>

        </div>
</section>


<!-- Info Left Section -->
<?php get_template_part( 'template-parts/commons/common', 'social' ); ?>


<?php// get_template_part( 'template-parts/page/home/home', 'contact' ); ?>
<?php
get_footer();